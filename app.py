from flask import Flask, render_template, request, flash, redirect, url_for,session
import psycopg2
from wtforms import Form, StringField, TextAreaField, PasswordField, validators

app = Flask(__name__)


try:
    conn = psycopg2.connect("dbname='testdb1' user='postgres' host='localhost' password='madhubala1987'")
except:
    print ("I am unable to connect to the database")

cur1 = conn.cursor()
cur1.execute("""SELECT * FROM usr""")
rows = cur1.fetchall()
for row in rows:
    print(row)

class RegistrationForm(Form):
    userid = StringField('userid', [validators.Length(min=1, max=10)])
    username = StringField('username', [validators.Length(min=1, max=20)])
    usermobile = StringField('usermobile', [validators.Length(min=1, max=10)])
    useremail = StringField('useremail', [validators.Length(min=1, max=50)])
    useraddress = StringField('useraddress', [validators.Length(min=5, max=30)])
    password = PasswordField('password', [validators.Length(min=6, max=20)])


@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm(request.form)
    if request.method == 'POST' and form.validate():
        cur = conn.cursor()
        userid = str(form.userid.data)
        username = str(form.username.data)
        usermobile = str(form.usermobile.data)
        useremail = str(form.useremail.data)
        useraddress = str(form.useraddress.data)
        password = str(form.password.data)
        cur.execute('INSERT INTO usr(userid, username, usermobile, useremail, useraddress) VALUES(%s, %s, %s, %s, %s)', (userid, username, usermobile, useremail, useraddress))
        cur.execute('INSERT INTO login(userid, password) VALUES(%s, %s)', (userid, password))
        conn.commit()
        flash('Succesfully submitted', 'success')
        return render_template('index.html')
    return render_template('register.html', form=form)


    


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        # Get Form Fields
        userid = str(request.form['userid'])
        password_candidate = request.form['password']
        cur = conn.cursor()
        cur.execute("SELECT password FROM login WHERE userid = %s", (userid,))
        result = cur.fetchall()
        print(result)
        if len(result) > 0 :
            pswrd = str(result[0][0])
            pswrd = pswrd.strip()
            if pswrd == password_candidate:
                usercur = conn.cursor()
                usercur.execute("SELECT * FROM usr WHERE userid = %s", (userid,))
                userDetails = usercur.fetchall()
                userName = str(userDetails[0][1])
                userName = userName.strip()
                userid = str(userDetails[0][0])
                print(userDetails)
                print(userid)
                userid = userid.strip()
                session['logged_in'] = True
                session['username'] = userName
                session['userid'] = userid

                return render_template('dashboard.html')
    return render_template('login.html')

@app.route('/account')
def account():
    cur = conn.cursor()
    userid = session['userid']
    print(userid)
    cur.execute('SELECT balance FROM accounts WHERE userid = %s', (userid,))
    result = cur.fetchall()
    print(result)
    balance = str(result[0][0])
    return render_template('account.html', result = balance)

@app.route('/loan', methods=['GET', 'POST'])
def loan():
    if request.method == 'POST':
        # Get Form Fields
        amount = int(request.form['amount'])
        cur = conn.cursor()
        userid = session['userid']
        checkcur = conn.cursor()
        checkcur.execute('SELECT * FROM loans WHERE userid = %s', (userid,))
        checkres = checkcur.fetchall()
        if len(checkres) > 0:
            cur.execute('UPDATE loans SET amount = %s WHERE userid = %s', (amount, userid))
        else:
            cur.execute('INSERT INTO loans(userid, amount) VALUES(%s, %s)', (userid, amount))
        conn.commit()
        return render_template('dashboard.html')
    return render_template('loan.html')

@app.route('/add', methods=['GET', 'POST'])
def route():
    if request.method == 'POST':
        # Get Form Fields
        amount = int(request.form['amount'])
        cur = conn.cursor()
        userid = session['userid']
        checkcur = conn.cursor()
        checkcur.execute('SELECT * FROM loans WHERE userid = %s', (userid,))
        checkres = checkcur.fetchall()
        if len(checkres) > 0:
            cur.execute('UPDATE loans SET amount = %s WHERE userid = %s', (amount, userid))
        else:
            cur.execute('INSERT INTO loans(userid, amount) VALUES(%s, %s)', (userid, amount))
        conn.commit()
        return render_template('dashboard.html')
    return render_template('add.html')

@app.route('/pay', methods=['GET', 'POST'])
def pay():
    if request.method == 'POST':
        toid = str(request.form['userid'])
        amount = int(request.form['amount'])
        userid = session['userid']
        cur = conn.cursor()
        checkcur = conn.cursor()
        checkcur.execute('SELECT * FROM debit')
        checkres = checkcur.fetchall()
        if len(checkres) > 0:
            cur.execute('INSERT INTO debit(debitid, userid, toid, amount) VALUES((SELECT MAX(creditid) FROM credit) + 1, %s, %s, %s)', (userid, toid, amount))
        else:
            cur.execute('INSERT INTO debit(debitid, userid, toid, amount) VALUES(1, %s, %s, %s)', (userid, toid, amount))
        conn.commit()
        return render_template('dashboard.html')
    return render_template('pay.html')

@app.route('/payments')
def payments():
    return render_template('payments.html')


@app.route('/dashboard')
def dashboard():
    return render_template('dashboard.html')

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/about')
def about():
    return render_template('about.html')



if __name__ == '__main__' :
    app.secret_key = 'super secret key'
    app.run(debug=True)